import model.Person;
import model.Transaction;
import repositories.TransactionRepository;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoLocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class CSVReportService {

    private final PersonsService personsService;
    private final TransactionRepository transactionRepository;
    private String head;
    private String body;

    public CSVReportService(PersonsService personsService, TransactionRepository transactionRepository) {
        this.personsService = personsService;
        this.transactionRepository = transactionRepository;
        this.head = new String();
        this.body = new String();
    }

    /**
     * Retrieve the average consumption (transaction amount) per @{@link model.Person}'s distinct roles during the last month
     * <p>
     * Note that roles are just tags that each person is assigned. ie 'student', 'gamer', 'athlete', 'parent'
     * a Person may have multiple roles or none.
     *
     * @return data in csv file format,
     * where the first line depict the roles
     * and the second line the average consumption per role
     * ie: (formatted example -- the actual output should be just comma separated)
     * |student, gamer, parent|
     * |10.50  , 20.10, 0     |
     */
    public String getAverageConsumptionPerRoleDuringTheLastMonth() {
        //get only the trasactions made in the range of a month
        List<Transaction> lastMonthTransactions = this.getLastMonthsTransactions();

        //calculates the transactions made from the persons that belong to every role available in the stream
        lastMonthTransactions
                .stream()
                .filter(Objects::nonNull)
                .map(Transaction::getEmailAddress)
                .map(personsService::getPersonByEmailAddress)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(Person::getRoles)
                .flatMap(Collection::stream)
                .distinct()
                .map(role -> getPersonsOfRole(role))
                .collect(Collectors.toList());

        //print the csv cutting the last ','
        String csv = this.head.substring(0, this.head.length() - 1) + "\n" + this.body.substring(0, this.body.length() - 1);
        System.out.println(csv);
        //return the csv
        return csv;

        // throw new UnsupportedOperationException();
    }

    /**
     * Filters out the transactions older than one month
     *
     * @return List of transactions
     */
    List<Transaction> getLastMonthsTransactions() {
        List<Transaction> transactions = transactionRepository.getTransactions();
        LocalDateTime from = LocalDateTime.now().minusMonths(1);
        ZonedDateTime zonedDateTime = from.atZone(ZoneId.of("Europe/Athens"));
        ZonedDateTime zonedDateTimeWZone = zonedDateTime.withZoneSameInstant(ZoneId.of("Europe/Athens"));

        List<Transaction> lastMonthTransactions = transactions
                .stream()
                .filter(transactionEntity -> {
                    return transactionEntity.getDate().isAfter(zonedDateTimeWZone.toLocalDateTime());
                })
                .collect(Collectors.toList());
        return lastMonthTransactions;
    }

    /**
     * Returns the transactions that belong to a provided email
     *
     * @return List of transactions
     */
    List<Transaction> getTransactionsByEmail(String email) {
        return this.getLastMonthsTransactions()
                .stream()
                .filter(Objects::nonNull)
                .filter(tra -> tra.getEmailAddress().equals(email))
                .collect(Collectors.toList());
    }

    /**
     * Returns the Persons that belong to a provided role
     *
     * @return List of Persons
     */
    List<Person> getPersonsOfRole(String role) {

        List<Person> personList = this.getLastMonthsTransactions()
                .stream()
                .filter(Objects::nonNull)
                .map(Transaction::getEmailAddress)
                .map(personsService::getPersonByEmailAddress)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(person -> person.getRoles().toString().contains(role))
                .distinct()
                .collect(Collectors.toList());

        System.out.println("Role: " + role);
        for (Person person : personList) {
            System.out.println(person.getEmailAddress());
            System.out.println(person.getRoles());
        }
        //construct the csv head
        this.head = head.concat(role + ",");
        //get the average transactions by role
        this.getAverageTransactionsByRole(personList, role);

        System.out.println("-----");

        return personList;
    }

    /**
     * Calculates the average transactions By role and constructs the csv body
     *
     * @param personList
     * @param role
     */
    void getAverageTransactionsByRole(List<Person> personList, String role) {
        OptionalDouble avtransactionsByRole = personList
                .stream()
                .map(Person::getEmailAddress)
                .map(email -> getTransactionsByEmail(email))
                .flatMap(Collection::stream)
                .mapToLong(Transaction::getAmount)
                .average();

        System.out.println("Average of role:" + role + " = " + avtransactionsByRole.getAsDouble());
        //construct the csv body
        this.body = body.concat(avtransactionsByRole.getAsDouble() + ","); //concatenate the body
    }
}
